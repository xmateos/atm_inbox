<?php

namespace ATM\InboxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use XLabs\ResultCacheBundle\Annotations as XLabsResultCache;

/**
 * @ORM\Entity
 * @ORM\Table(name="conversation_message")
 * @XLabsResultCache\Clear(onFlush={}, {
 *      @XLabsResultCache\Key(onFlush={"insert","update","delete"}, type="literal", method="clearConversation")
 * })
*/
class ConversationMessage{
    const RESULT_CACHE_CONVERSATION_ITEM = 'atm_conversation_';
    const RESULT_CACHE_CONVERSATION_ITEM_TTL = 36000000;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="Conversation", inversedBy="conversationMessages")
    */
    protected $conversation;

    /**
     * @ORM\ManyToOne(targetEntity="Message", inversedBy="conversationMessages")
    */
    protected $message;

    /**
     * @ORM\Column(name="is_read", type="boolean", nullable=false)
     */
    protected $isRead;


    protected $user;

    public function __construct(){
        $this->isRead = false;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getConversation()
    {
        return $this->conversation;
    }

    public function setConversation($conversation)
    {
        $this->conversation = $conversation;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getIsRead()
    {
        return $this->isRead;
    }

    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;
    }

    public function clearConversation(){
        return array(
            self::RESULT_CACHE_CONVERSATION_ITEM.$this->conversation->getId(),
            self::RESULT_CACHE_CONVERSATION_ITEM.$this->conversation->getId().'_hydration'
        );
    }
}