<?php

namespace ATM\InboxBundle\Event;

use Symfony\Component\EventDispatcher\Event;

class CheckForSpam extends Event
{
    const NAME = 'atm_inbox_check_spam.event';

    private $message;

    public function __construct($message)
    {
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }
}