An Inbox Manager System

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require atm/inboxbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new ATM\InboxBundle\ATMInboxBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
atm_inbox:
    resource: "@ATMInboxBundle/Resources/config/routing.yml"
    prefix:   /
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
atm_inbox:
    class:
        model:
            conversation: Your conversation class namespace
            conversation_message: Your conversation message namespace
            message: Your message namespace
            user: Your User namespace
```


### Inheritance of classes ###

In order to use the ATMInboxBundle classes you will have to create your own Conversation, ConversationMessage and 
Message classes that will extend from the ATMInboxBundle's Conversation, ConversationMessage and Message abstract
classes as shown bellow:

```php
namespace CoreBundle\Entity;

use ATM\InboxBundle\Entity\Conversation as BaseConversation;

class Conversation extends BaseConversation{}
```
```php
namespace CoreBundle\Entity;

use ATM\InboxBundle\Entity\ConversationMessage as BaseConversationMessage;

class ConversationMessage extends BaseConversationMessage{}
```
```php
namespace CoreBundle\Entity;

use ATM\InboxBundle\Entity\Message as BaseMessage;

class ConversationMessage extends BaseConversationMessage{}
```

### Twig Extension ###
You can use the following twig extension to know how many unread message the user has:

```twig
{{ getUnreadMessages(app.user.id) }}
```