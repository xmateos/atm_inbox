<?php

namespace ATM\InboxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use ATM\InboxBundle\Entity\CustomMessage;
use XLabs\TrumboWYGBundle\Form\TrumboWYGType;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class CustomMessageType extends AbstractType
{
    public $router;
    public $tokenStorage;
    public $web_folder;
    public $atm_inbox_config;

    public function __construct(RouterInterface $router, TokenStorageInterface $tokenStorage, KernelInterface $kernel, $atm_inbox_config)
    {
        $this->router = $router;
        $this->tokenStorage = $tokenStorage;
        $this->web_folder = $kernel->getRootDir().'/../web/';
        $this->atm_inbox_config = $atm_inbox_config;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $user = $this->tokenStorage->getToken()->getUser();
        $builder
            ->add('type',ChoiceType::class,array(
                'label' => 'Select a DM type',
                'label_attr' => ['class' => 'form-label'],
                'choices' => array(
                    'New Users' => CustomMessage::NEW_USER,
                    'New Models' => CustomMessage::NEW_MODEL,
                    'Model Max Poinst Reached' => CustomMessage::MODEL_MAX_POINTS_REACHED
                ),
                //'data' => 'date',
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'placeholder' => false,
                //'attr' => ['class' => 'form-select form-select-sm']
            ))
            ->add('subject',TextType::class,array('required'=>true))
            ->add('body', TrumboWYGType::class,array(
                'required' => true,
                'label' => false,
                'attr' => array(
                    'placeholder' => ''
                ),
                'plugin_options' => [
                    'btns' => [['bold', 'italic', 'underline'], ['foreColor', 'backColor'], ['link'], ['upload'], ['emoji']],
                    'autogrow' => true,
                    'tagsToRemove' => ['script', 'link', 'iframe'],
                    'plugins' => [
                        'upload' => [
                            'data' => [
                                [
                                    'name' => 'folder',
                                    'value' => $this->web_folder.'media/'.$this->atm_inbox_config['media_folder'].'/'.$user->getFolder().'/img_messages'
                                ]
                            ]
                        ]
                    ]
                ]
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => CustomMessage::class,
        ));
    }

    public function getBlockPrefix()
    {
        return 'atminbox_bundle_custom_message_type';
    }
}