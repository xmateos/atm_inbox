<?php

namespace ATM\InboxBundle\EventListener;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
class MessageRelationSubscriber implements EventSubscriber
{
    private $config;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function getSubscribedEvents()
    {
        return array(
            Events::loadClassMetadata
        );
    }

    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $metadata = $eventArgs->getClassMetadata();

        if ($metadata->getName() != 'ATM\InboxBundle\Entity\Message') {
            return;
        }

        $metadata->mapManyToOne(array(
            'targetEntity' => $this->config['class']['model']['user'],
            'fieldName' => 'receiver',
            'joinColumns' => array(
                array(
                    'name' => 'receiver_id',
                    'referencedColumnName' => 'id'
                )
            )
        ));

        $metadata->mapManyToOne(array(
            'targetEntity' => $this->config['class']['model']['user'],
            'fieldName' => 'author',
            'joinColumns' => array(
                array(
                    'name' => 'author_id',
                    'referencedColumnName' => 'id'
                )
            )
        ));
    }
}