<?php

namespace ATM\InboxBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder->root('atm_inbox')
                        ->children()
                            ->arrayNode('class')->isRequired()
                                ->children()
                                    ->arrayNode('model')->isRequired()
                                        ->children()
                                            ->scalarNode('user')->isRequired()->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->arrayNode('send_to_all_roles')
                                ->requiresAtLeastOneElement()
                                ->useAttributeAsKey('name')
                                ->prototype('scalar')->end()
                            ->end()
                            ->scalarNode('media_folder')->isRequired()->end()
                            ->scalarNode('no_reply_bottom_line')->defaultValue('<br /><br /><span style="font-size: .7em;">This message is automatically generated, you can not reply to it</span>')->end()
                    ->end();

        return $treeBuilder;
    }
}
